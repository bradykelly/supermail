function addMailItem(args) {
	var newItem = jQuery("#mail-item-template").clone().removeAttr("id");
	jQuery(newItem).find("img.mail-item-delete").click(function() {
		removeMailItem(jQuery(this).parent());
	});
	jQuery("#mail-item-list").append(newItem);
	if (args["offset"]) {
		jQuery(newItem).find("input.days-edit").val(args.offset);
	}
	if (args["templateId"]) {
		jQuery(newItem).find("select.mail-item-select").val(args.templateId);
	}
}

function removeMailItem(listItemElement) {
	jQuery(listItemElement).remove();
}

function serializeMailSeries() {
	var series = [];
	jQuery("#mail-item-list li.mail-item[id!='mail-item-template']").each(
			function() {
				var item = {};
				item.offset = jQuery(this).find("input.days-edit").val();
				item.templateId = jQuery(this).find("select.mail-item-select")
						.val();
				series.push(item);
			});
	jQuery("input[name='welcome_series']").val(jQuery.toJSON(series));
}

function deserializeMailSeries() {
	var json = jQuery("input[name='welcome_series']").val();
	if (json.length == 0) {
		return false;
	}
	var series = jQuery.evalJSON(json);
	for ( var i = 0; i < series.length; i++) {
		addMailItem({
			"offset" : series[i].offset,
			"templateId" : series[i].templateId
		});
	}
}

jQuery(function() {

	jQuery("#mail-item-add").click(function() {
		addMailItem();
	});
	deserializeMailSeries();
	jQuery("#submit").click(function() {
		serializeMailSeries();
		return true;
	});
});
