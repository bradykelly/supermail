jQuery(function() {
	jQuery("form[name='post']").attr("enctype", "multipart/form-data");
	jQuery(
			"a[href='edit.php?post_type=wpsm_mail_template'][class*='wp-first-item']")
			.html("All Templates");
	var mailMeta = jQuery("#wpsm-mail-meta");
	if (mailMeta.length > 0) {
		jQuery("#titlediv").css("display", "none");
		mailMeta.insertAfter("#titlediv");
		jQuery("#wpsm_mail_title").change(
				function() {
					jQuery("#title").attr("value",
							jQuery("#wpsm_mail_title").attr("value"));
				});
		jQuery("#title")
				.attr("value", jQuery("#wpsm_mail_title").attr("value"));

		if (jQuery("#file-name :text").val().length == 0) {
			jQuery("#file-upload").css("display", "block");
			jQuery("#file-name").css("display", "none");
		} else {
			jQuery("#file-upload").css("display", "none");
			jQuery("#file-name").css("display", "block");
		}
	}
});
