function clearQueue() {
	jQuery("#msg").html("");
	jQuery("#queue-clear").attr("disabled", "disabled");
	jQuery("#queue-clear-ajax").css("display", "inline");
	jQuery.ajax({
		type : 'POST',
		data : {
			action : 'process_clear'
		},
		url : ajaxurl,
		success : function(value) {
			jQuery("table#mail_queue tbody tr").remove();
			jQuery("table#mail_queue tbody").html(value);
			jQuery("#queue-clear").removeAttr("disabled");
			jQuery("#queue-clear-ajax").css("display", "none");
		}
	});
}

function processQueue() {
	jQuery("#msg").html("");
	jQuery("#queue-process").attr("disabled", "disabled");
	jQuery("#queue-process-ajax").css("display", "inline");
	var process_size = jQuery("#process_size").val();
	jQuery.ajax({
		type : 'POST',
		data : {
			action : 'process_queue',
			process_size : process_size
		},
		url : ajaxurl,
		success : function(value) {
			jQuery("table#mail_queue tbody tr").remove();
			jQuery("table#mail_queue tbody").html(value);
			jQuery("#queue-process").removeAttr("disabled");
			jQuery("#queue-process-ajax").css("display", "none");
		}
	});
}

jQuery(function() {
	jQuery("#queue-process-ajax").css("display", "none");
	jQuery("#queue-clear-ajax").css("display", "none");
	jQuery("#queue-process").click(function() {
		processQueue();
		return false;
	});
	jQuery("#queue-clear").click(function() {
		clearQueue();
		return false;
	});
});