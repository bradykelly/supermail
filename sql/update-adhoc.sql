SELECT 
--       pp.ID,
--        pp.post_title,
--        pp.post_status,
--        pm.meta_key,
--        pm.meta_value
 pp.ID post_id
 , 'is_ad_hoc' meta_key
 , 
  FROM    wp_posts pp
       LEFT JOIN
          wp_postmeta pm
       ON pp.ID = pm.post_id AND pm.meta_key = 'is_ad_hoc'
 WHERE     1 = 1
       AND pp.post_type = 'wpsm_mail_template'
       AND pp.post_status IN ('private', 'publish')
       AND pp.post_parent = 0
       and pm.meta_key is null
ORDER BY id DESC


select * from wp_postmeta