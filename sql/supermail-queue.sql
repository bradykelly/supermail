DROP TABLE IF EXISTS  `wp_supermail_queue` ;

CREATE TABLE `wp_supermail_queue` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_queued` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `template_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `date_to_send` datetime DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `date_sent` datetime DEFAULT NULL,
  `status_desc` varchar(20) DEFAULT NULL,
  KEY `wp_supermail_queue.PRIMARY` (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


