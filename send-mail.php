<?php

wp_enqueue_style ( 'wpsm_send_mail', WPSM_Plugin_Integrator::styleUrl ( 'mail-send.css' ) );
wp_enqueue_style ( WPSM_STYLE_VALIDATE, WPSM_Plugin_Integrator::styleUrl ( WPSM_STYLE_VALIDATE ) );
wp_enqueue_script ( WPSM_SCRIPT_VALIDATE, WPSM_Plugin_Integrator::scriptUrl ( WPSM_SCRIPT_VALIDATE ) );
wp_enqueue_script ( 'wpsm_send_mail', WPSM_Plugin_Integrator::scriptUrl ( 'send-mail.js' ), array (WPSM_SCRIPT_VALIDATE ) );

$validator = new WPSM_Validator ();
if (isset ( $_POST ["wpsm_is_post"] )) {
	
	// TODO Integrate server validation messages with client validation.
	
	if (strlen ( trim ( $_POST ['wpsm_from'] ) ) == 0) {
		$validator->addErrorMessage ( "The message has no 'From' address. The administrator can set a default for this on the SuperMail 'Settings' page." );
	}
	if (strlen ( trim ( $_POST ['wpsm_subject'] ) ) == 0) {
		$validator->addErrorMessage ( "The message has no subject." );
	}
	if (strlen ( trim ( $_POST ['wpsm_body'] ) ) == 0) {
		$validator->addErrorMessage ( "The message has no body." );
	}
	
	$wpsm_user_group = strip_tags ( $_POST ['user_group'] );
	$wpsm_from = strip_tags ( $_POST ['wpsm_from'] );
	$wpsm_subject = strip_tags ( $_POST ['wpsm_subject'] );
	$wpsm_body = $_POST ['wpsm_body'];
	
	$wpsm_attachment = "";
	if (! empty ( $_FILES [WPSM_MAIL_META_ATTACHMENT] ['name'] )) {
		
		$upload = wp_handle_upload ( $_FILES [WPSM_MAIL_META_ATTACHMENT], array ('test_form' => false ) );
		$nice_path = str_replace ( "\\", "/", $upload ['file'] );
		if ((isset ( $upload ['error'] )) && ($upload ['error'] != 'Specified file failed upload test.')) {
			$err = $upload ['error'];
			error_log ( "Attachment upload aborted: $err" );
			return;
		} else {
			$wpsm_attachment = $nice_path;
		}
	}
	
	error_log ( "Ad hoc sending with subject: $wpsm_subject, attachment: $wpsm_attachment" );
	
	if (! $validator->hasErrors ()) {
		$wpsm_title = null;
		error_log ( "Sending: $wpsm_body" );
		if ($wpsm_user_group !== 'all') {
			// TODO Make one send method that can interpret the 'all' group itself.
			WPSM_SuperMail::sendAdHocMailToUserGroup ( $wpsm_user_group, $wpsm_from, $wpsm_subject, $wpsm_body, true, $wpsm_title, $wpsm_attachment );
		} else {
			WPSM_SuperMail::sendAdHocMailToAllUsers ( $wpsm_from, $wpsm_subject, $wpsm_body, $wpsm_title, $wpsm_attachment );
		}
	}
} else {
	$wpsm_from = esc_html ( get_option ( WPSM_OPTION_DEFAULT_FROM ) );
	if (strlen ( $wpsm_from ) == 0) {
		$wpsm_from = esc_html ( get_option ( 'admin_email' ) );
	}
	$wpsm_subject = '';
	$wpsm_body = '';
	$wpsm_attachment = '';
}
?>
<div class="wrap">
	<form id="wpsm-send-mail" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST"
		enctype="multipart/form-data"></form>
	<form id="wpsm-send-mail" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST"
		enctype="multipart/form-data">
		<div id="wpsm-mail-send" style="width: 800px;">
			<h2>SuperMail <?php echo 'v' . $wpsm_plugin_object->getVersion(); ?></h2>
			<div style="width: 800px;">
				<input type="submit" name="submit" style="float: right;" value="<?php _e('Send Email', WPSM_I18N_DOMAIN); ?> &raquo;" />
				<h3>Send Mail to All Users</h3>
				<p class="description">Send a mail to all users except those that have opted out of bulk emails on their user profiles.</p>
			</div>
			<?php if ($validator->hasErrors()): ?>
            	<div id="wpsm-server-error-container">
				<h4 class="error">SuperMail cannot send this mail because of the following problems:</h4>
				<ul>
                    	<?php foreach ($validator->getErrorMessages() as $msg): ?>
                        	<li class="error"><?php echo $msg; ?></li>
                    	<?php endforeach; ?>
                	</ul>
			</div>
			<?php endif; ?>
			<div id="wpsm-top-error-container">
				<h4 class="error">SuperMail cannot send this mail because of the following problems:</h4>
				<ul>
				</ul>
			</div>
			<input type="hidden" name="wpsm_is_post" value="true" />
			<table class="form-table">
				<tr>
					<th scope="row" valign="top"><label for="agent_type"><?php _e("User Group", WPSM_I18N_DOMAIN); ?></label></th>
					<td><select name="user_group" id="user_group">
							<option value="all">All Users</option>
									<?php
									foreach ( WPSM_Plugin_Integrator::getUserRoles () as $group ) {
										echo ('<option value="' . $group . '">' . $group . '</option>');
										// echo $mta;
									}
									?>
								</select></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="wpsm_from"><?php _e("From", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" id="wpsm_from" name="wpsm_from" class="required" value="<?php echo esc_html($wpsm_from); ?>"
						style="width: 100%;" /></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="wpsm_subject"><?php _e("Subject", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" id="wpsm_subject" name="wpsm_subject" class="required" value="<?php echo esc_html($wpsm_subject); ?>"
						style="width: 100%;" /></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="<?php echo WPSM_MAIL_META_ATTACHMENT; ?>"> <?php _e('Attachment', WPSM_I18N_DOMAIN); ?></label></th>
					<td id="file-upload"><input type="file" style="width: 100%;" name="<?php echo WPSM_MAIL_META_ATTACHMENT; ?>"
						id="<?php echo WPSM_MAIL_META_ATTACHMENT; ?>" />
				
				</tr>
				<tr>
					<td colspan="2"><span class="description caution">We recommend that attachments are kept below 200kb. If your attachments are large, we
							suggest that you upload them to your media library and insert a link to the media in your mail. This will reduce the load on your
							server and speed up delivery of mail to your users. For more information on how to upload and add a link, visit <a
							href="http://www.supermail.com">http://www.supermail.com</a>
					</span></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="Content"><?php _e("Body", WPSM_I18N_DOMAIN); ?></label></th>
					<td>
						<div id="mail-content-editor">							
							<?php wp_editor($wpsm_body, 'wpsm_body', array('wpautop' => true, 'textarea_name' => 'wpsm_body'))?>
                        </div>
					</td>
				</tr>
			</table>
			<div id="wpsm-bottom-error-container">
				<h4 class="error">SuperMail cannot send this mail. Please see the top of the page for details.</h4>
			</div>
			<p class="submit">
				<input type="submit" name="submit" value="<?php _e('Send Email', WPSM_I18N_DOMAIN); ?> &raquo;" />
			</p>
		</div>
	</form>
</div>
