<?php

require_once 'code/class-wpsm-template.php';


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<link href="MailSeries.css" rel="stylesheet" type="text/css" />
<script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

</head>
<body>
	<div id="mail-series-editor">
		<a id="mail-item-add" href="#"> <img src="Images/add-medium.png"
			alt="Click to add a new mail item." title="New mail item" />&nbsp;Add
			a mail template
		</a>
		<ol id="mail-item-list">
			<li id="mail-item-template" class="mail-item"><label> After</label> <input
				type="text" class="days-edit" /> <label> days, send</label> <select
				class="mail-item-select">
				<?php ?>
			</select> <img src="Images/delete-medium.png"
				class="mail-item-delete" alt="Remove button"
				title="Remove this mail item" /></li>
		</ol>
	</div>
</body>
</html>
