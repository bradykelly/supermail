<?php

function wpsm_user_profile_update() {
	global $_POST;
	
	$user = wp_get_current_user ();
	
	// TODO Find out why constant is not working.
	$field_name = 'wpsm_refuse_bulk_mail';
	
	if (isset ( $_POST [$field_name] )) {
		update_user_meta ( $user->ID, $field_name, true );
	} else {
		delete_user_meta ( $user->ID, $field_name );
	}
}

function wpsm_checkbox_checked($bool_val) {
	return $bool_val ? 'checked="checked"' : '';
}
?>
<h3><?php echo("Email Preferences"); ?></h3>
<table class="form-table">
	<tbody>
		<tr>
			<th>Opt Out of Bulk Emails</th>
			<td><input type="checkbox"
				name="<?php echo(WPSM_USER_META_REFUSE_BULK_MAIL); ?>"
				id="<?php echo WPSM_USER_META_REFUSE_BULK_MAIL; ?>" value="true"
				<?php echo wpsm_checkbox_checked(get_user_meta(wp_get_current_user()->ID, WPSM_USER_META_REFUSE_BULK_MAIL)); ?> />
				<span class="description"><?php echo('I want to opt out of receiving emails sent to all users (but still accept emails sent only to me).'); ?></span>
			</td>
		</tr>
		<tr>
			<th></th>
			<td><p class="description">
					This site uses the <a href="http://wpsupermail.com/"
						target="_blank">SuperMail Plugin</a> to improve email
					communications with our users. You can tick the check-box above to
					opt out of receiving any emails that SuperMail sends to all users.
				</p>
				<p class="description">However please be aware that SuperMail cannot
					guarantee that you will not receive bulk emails sent by other,
					rogue plug-ins.</p></td>
		</tr>

	</tbody>
</table>
