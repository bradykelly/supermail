<?php
// TODO Find out how to stop this header comment being formatted.
/*
 * Plugin Name: SuperMail Plugin URI: http://example.com/wordpress-plugins/my-plugin Description: Bulk mail plugin. Send ad-hoc and
 * scheduled mails to all users and groups of users. Version: 0.10 Author: Brady Kelly Author URI: http://thepraxis.co.za License: GPLv2s
 */

$wpsm_base_path = str_replace("\\", "/", plugin_dir_path(__FILE__));
function wpsm_autoloader($class_name)
{
    global $wpsm_base_path;

    if (strpos($class_name, "WPSM_") === 0) {
        $class_path = $wpsm_base_path . "code/class-" . str_replace("_", "-", strtolower($class_name)) . ".php";
        include $class_path;
    }
}

spl_autoload_register("wpsm_autoloader");

$wpsm_plugin_object = new WPSM_Plugin_Integrator ();
$wpsm_plugin_object->activate();
?>