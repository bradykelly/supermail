<?php
require_once 'code/class-wpsm-template.php';

wp_enqueue_style ( 'settingsStyle', WPSM_Plugin_Integrator::styleUrl ( 'settings.css' ) );
wp_enqueue_style ( WPSM_STYLE_VALIDATE, WPSM_Plugin_Integrator::styleUrl ( WPSM_STYLE_VALIDATE ) );
wp_enqueue_style ( 'mailSeries', WPSM_Plugin_Integrator::styleUrl ( 'mail-series.css' ) );
wp_enqueue_script ( WPSM_SCRIPT_VALIDATE, WPSM_Plugin_Integrator::scriptUrl ( WPSM_SCRIPT_VALIDATE ) );
// wp_enqueue_script ( 'jquery.validate.defaults', WPSM_Plugin_Integrator::scriptUrl ( 'jquery.validate.defaults.js' ), array
// (WPSM_SCRIPT_VALIDATE ) );
wp_enqueue_script ( 'wpsm-settings', WPSM_Plugin_Integrator::scriptUrl ( 'settings.js' ), array (WPSM_SCRIPT_VALIDATE ) );
wp_enqueue_script ( 'jqueryJson', WPSM_Plugin_Integrator::scriptUrl ( 'jquery.json-2.3.min.js' ) );
wp_enqueue_script ( 'mailSeries', WPSM_Plugin_Integrator::scriptUrl ( 'mail-series.js' ) );

$wpsm_template_list = wpsm_Template::getAllTemplateHeaders ();
if (isset ( $_POST ['wpsm_is_post'] )) {
	
	// TODO Server side validation.
	
	extract ( $_POST, EXTR_PREFIX_ALL, 'wpsm' );
	$wpsm_welcome_series = stripslashes ( stripslashes ( $wpsm_welcome_series ) );
	update_option ( 'wpsm_license_email', wp_strip_all_tags ( $wpsm_license_email ) );
	update_option ( 'wpsm_license_key', wp_strip_all_tags ( $wpsm_license_key ) );
	update_option ( WPSM_OPTION_DEFAULT_FROM, wp_strip_all_tags ( $wpsm_default_from ) );
	update_option ( WPSM_OPTION_WELCOME_SERIES, $wpsm_welcome_series );
	
	update_option ( WPSM_OPTION_LEGAL_NAME, $wpsm_legal_name );
	update_option ( WPSM_OPTION_LEGAL_ADDRESS, $wpsm_legal_address );
	update_option ( WPSM_OPTION_LEGAL_STATE, $wpsm_legal_state );
	update_option ( WPSM_OPTION_LEGAL_ZIP, $wpsm_legal_zip );
	update_option ( WPSM_OPTION_LEGAL_COUNTRY, $wpsm_legal_country );
	
	update_option ( WPSM_MAIL_PROCESS_CRON, $wpsm_process_cron );
	update_option ( WPSM_MAIL_PROCESS_FREQUENCY, $wpsm_process_frequency );
	update_option ( WPSM_MAIL_PROCESS_MEASURE, $wpsm_process_measure );
	$mail_cron_internal = WPSM_Scheduler::getIntervalSeconds ( $wpsm_process_measure, $wpsm_process_frequency );
	$scheduler = new WPSM_Scheduler ();
	$scheduler->changeScheduledHook(WPSM_CRONHOOK_MAIL, $mail_cron_internal);
	update_option ( WPSM_MAIL_PROCESS_INTERVAL, $mail_cron_internal );
	update_option ( WPSM_MAIL_PROCESS_SIZE, $wpsm_process_size );
	
	update_option ( WPSM_MAIL_AGENT_TYPE, $wpsm_agent_type );
	update_option ( WPSM_MAIL_AGENT_SMTP_HOST, $wpsm_smtp_host );
	update_option ( WPSM_MAIL_AGENT_SMTP_PORT, $wpsm_smtp_port );
	update_option ( WPSM_MAIL_AGENT_SMTP_ENCRYPT, $wpsm_smtp_encryption );
	update_option ( WPSM_MAIL_AGENT_SMTP_USER_NAME, $wpsm_smtp_user_name );
	update_option ( WPSM_MAIL_AGENT_SMTP_PASSWORD, $wpsm_smtp_password );
	update_option ( WPSM_MAIL_AGENT_SENDMAIL_COMMAND, $wpsm_sendmail_command );
	
	if (isset ( $wpsm_test_agent_check )) {
		$mta = new WPSM_Mailer ();
		if ($mta->sendTestMail ( get_option ( 'admin_email' ) ) === 0) {
			echo "Failed: ";
		}
	}

} else {
	$wpsm_license_email = wp_strip_all_tags ( get_option ( 'wpsm_license_email' ) );
	$wpsm_license_key = wp_strip_all_tags ( get_option ( 'wpsm_license_key' ) );
	$wpsm_default_from = wp_strip_all_tags ( get_option ( WPSM_OPTION_DEFAULT_FROM ) );
	$wpsm_welcome_series = get_option ( WPSM_OPTION_WELCOME_SERIES );
	
	$wpsm_legal_name = wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_NAME ) );
	$wpsm_legal_address = wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_ADDRESS ) );
	$wpsm_legal_state = wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_STATE ) );
	$wpsm_legal_zip = wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_ZIP ) );
	$wpsm_legal_country = wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_COUNTRY ) );
	
	$wpsm_process_cron = wp_strip_all_tags ( get_option ( WPSM_MAIL_PROCESS_CRON, false ) );
	$wpsm_process_frequency = wp_strip_all_tags ( get_option ( WPSM_MAIL_PROCESS_FREQUENCY, 30 ) );
	$wpsm_process_measure = wp_strip_all_tags ( get_option ( WPSM_MAIL_PROCESS_MEASURE, 'seconds' ) );
	$wpsm_process_size = wp_strip_all_tags ( get_option ( WPSM_MAIL_PROCESS_SIZE, 5 ) );
	
	$wpsm_agent_type = wp_strip_all_tags ( get_option ( WPSM_MAIL_AGENT_TYPE ) );
	$wpsm_smtp_host = wp_strip_all_tags ( get_option ( WPSM_MAIL_AGENT_SMTP_HOST ) );
	$wpsm_smtp_port = wp_strip_all_tags ( get_option ( WPSM_MAIL_AGENT_SMTP_PORT ) );
	$wpsm_smtp_encryption = wp_strip_all_tags ( get_option ( WPSM_MAIL_AGENT_SMTP_ENCRYPT ) );
	$wpsm_smtp_user_name = wp_strip_all_tags ( get_option ( WPSM_MAIL_AGENT_SMTP_USER_NAME ) );
	$wpsm_smtp_password = wp_strip_all_tags ( get_option ( WPSM_MAIL_AGENT_SMTP_PASSWORD ) );
	$wpsm_sendmail_command = wp_strip_all_tags ( get_option ( WPSM_MAIL_AGENT_SENDMAIL_COMMAND ) );
}
?>
<!DOCTYPE html>
<div class="wrap">
	<h2>SuperMail <?php echo 'v' . $wpsm_plugin_object->getVersion(); ?> Settings</h2>
	<div style="width: 800px;">
		<span class="description">Required fields are marked with a </span><span class="required-mark">&nbsp;*</span>
		<div id="wpsm-top-error-container">
			<h4 class="error">SuperMail cannot update its settings. Please ensure all required fields are filled in.</h4>
			<ul>
			</ul>
		</div>
		<form id="wpsm-settings" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="POST">
			<input type="hidden" name="wpsm_is_post" value="true" />
			<table class="form-table">
				<tr>
					<td colspan="2"><input type="submit" style="float: right;" name="submit" id="submit" class="button-primary" value="Save Changes" />
						<h3>Mail Queue</h3></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="process_cron"><?php _e("Enable background mailing", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="checkbox" name="process_cron" value="1" <?php echo($wpsm_process_cron == true ? ' checked="checked"' : "");  ?> /></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="process_frequency"><?php _e("Process every", WPSM_I18N_DOMAIN); ?><span class="required-mark">&nbsp;*</span></label></th>
					<td><input type="text" name="process_frequency" class="required" value="<?php echo format_to_edit($wpsm_process_frequency); ?>"
						style="width: 40px;" /><span class="required-mark">&nbsp;*&nbsp;</span><select name="process_measure" id="process_measure">
							<option value="seconds" <?php echo ("seconds" == $wpsm_process_measure ? ' selected="selected"' : ""); ?>>Seconds</option>
							<option value="minutes" <?php echo ("minutes" == $wpsm_process_measure ? ' selected="selected"' : ""); ?>>Minutes</option>
							<option value="hours" <?php echo ("hours" == $wpsm_process_measure ? ' selected="selected"' : ""); ?>>Hours</option>
							<option value="days" <?php echo ("days" == $wpsm_process_measure ? ' selected="selected"' : ""); ?>>Days</option>
					</select></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="process_size"><?php _e("Send mail limit", WPSM_I18N_DOMAIN); ?><span class="required-mark">&nbsp;*</span></label></th>
					<td><input type="text" name="process_size" class="required" value="<?php echo format_to_edit($wpsm_process_size); ?>"
						style="width: 40px;" /></td>
				</tr>
				<tr>
					<td colspan="2"><span class="description caution">Please check your web host's mail policy for mailing restrictions before saving these
							settings.</span></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" style="float: right;" name="submit" id="submit" class="button-primary" value="Save Changes" />
						<h3>Compliance</h3></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="legal_name"><?php _e("Company Name", WPSM_I18N_DOMAIN); ?><span class="required-mark">&nbsp;*</span></label></th>
					<td><input type="text" name="legal_name" class="required" value="<?php echo format_to_edit($wpsm_legal_name); ?>" style="width: 100%;" />
					</td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="legal_address"><?php _e("Address", WPSM_I18N_DOMAIN); ?><span class="required-mark">&nbsp;*</span></label>
					</th>
					<td><input type="text" name="legal_address" class="required" value="<?php echo format_to_edit($wpsm_legal_address); ?>"
						style="width: 100%;" /></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="legal_state"><?php _e("State / Province", WPSM_I18N_DOMAIN); ?><span class="required-mark">&nbsp;*</span></label></th>
					<td><input type="text" name="legal_state" class="required" value="<?php echo format_to_edit($wpsm_legal_state); ?>"
						style="width: 150px;" /> <label for="legal_zip"><?php _e("Zip / Postal Code", WPSM_I18N_DOMAIN); ?><span class="required-mark">*</span></label>
						<input type="text" name="legal_zip" class="required" value="<?php echo format_to_edit($wpsm_legal_zip); ?>" style="width: 70px;"></input></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="legal_country"><?php _e("Country", WPSM_I18N_DOMAIN); ?><span class="required-mark">&nbsp;*</span></label>
					</th>
					<td><select name="legal_country" class="required">
                        <?php
																								foreach ( WPSM_CountryList::listAll () as $country ) {
																									echo ('<option value="' . $country ["code"] . '"' . ($country ["code"] == $wpsm_legal_country ? ' selected="selected"' : "") . '>' . $country ["name"] . '</optiom>');
																								}
																								?>
                    </select></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" style="float: right;" name="submit" id="submit" class="button-primary" value="Save Changes" />
						<h3>Mail Transport</h3></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="agent_type"><?php _e("Mail Transport Type", WPSM_I18N_DOMAIN); ?></label></th>
					<td><select name="agent_type" id="agent_type">
                        <?php
																								foreach ( WPSM_Mail_Transfer_Agent::listAgentTypes () as $mta ) {
																									echo ('<option value="' . $mta . '"' . ($mta == $wpsm_agent_type ? ' selected="selected"' : "") . '>' . $mta . '</option>');
																								}
																								?>
                    </select> <label for="test_agent_check">Test this agent when I save these settings.</label> <input type="checkbox"
						name="test_agent_check" /></td>
				</tr>
				<tr class="mta smtp">
					<th scope="row" valign="top"><label for="smtp_host"><?php _e("SMTP Host", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" name="smtp_host" value="<?php echo format_to_edit($wpsm_smtp_host); ?>" style="width: 100%;" />
				
				</tr>
				<tr class="mta smtp">
					<th scope="row" valign="top"><label for="smtp_port"><?php _e("SMTP Port", WPSM_I18N_DOMAIN); ?></label></th>
					<td><label for="smtp_port">Port</label> <input type="text" name="smtp_port" value="<?php echo format_to_edit($wpsm_smtp_port); ?>" /> <label
						for="smtp_encryption"> Encryption</label> <select name="smtp_encryption" id="smtp_encryption">
                        <?php
																								foreach ( WPSM_Smtp_Transfer_Agent::listEncryptionStreams () as $enc ) {
																									echo ('<option value="' . $enc . '"' . ($enc == $wpsm_smtp_encryption ? ' selected="selected"' : "") . '>' . $enc . '</option>');
																								}
																								?>
                    </select>
				
				</tr>
				<tr class="mta smtp">
					<th scope="row" valign="top"><label for="smtp_user_name"><?php _e("SMTP User Name", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" name="smtp_user_name" value="<?php echo format_to_edit($wpsm_smtp_user_name); ?>" style="width: 100%;" />
				
				</tr>
				<tr class="mta smtp">
					<th scope="row" valign="top"><label for="smtp_password"><?php _e("SMTP Password", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" name="smtp_password" value="<?php echo format_to_edit($wpsm_smtp_password); ?>" style="width: 100%;" />
				
				</tr>
				<tr class="mta sendmail">
					<th scope="row" valign="top"><label for="sendmail_command"><?php _e("SENDMAIL Command", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" name="sendmail_command" value="<?php echo format_to_edit($wpsm_sendmail_command); ?>" style="width: 100%;" />
				
				</tr>
				<tr>
					<td colspan="2"><input type="submit" style="float: right;" name="submit" id="submit" class="button-primary" value="Save Changes" />
						<h3>Mail Templates</h3></td>
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="default_from"><?php _e("Default Sender Address:", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" name="default_from" value="<?php echo format_to_edit($wpsm_default_from); ?>" style="width: 100%;" />
				
				</tr>
				<tr>
					<td></td>
					<td><span class="description">Leave this blank to use whatever is currently the administrator
						address. This is currently <?php echo get_option('admin_email'); ?></span>
				
				</tr>
				<tr>
					<th scope="row" valign="top"><label for="welcome_template"><?php _e("New Member Mail Series", WPSM_I18N_DOMAIN); ?></label></th>
					<td>
						<div id="mail-series-editor">
							<a id="mail-item-add" href="#"> <img src=<?php echo WPSM_Plugin_Integrator::imageUrl("add-medium.png")?>
								alt="Click to add a new mail item." title="New mail item" />&nbsp;Add a mail template
							</a> <input type="hidden" name="welcome_series" value='<?php echo $wpsm_welcome_series; ?>' />
							<ul id="mail-item-list">
								<li id="mail-item-template" class="mail-item"><label> After</label> <input type="text" class="days-edit" /> <label> days, send</label>
									<select class="mail-item-select">
                                        <?php
																																								foreach ( WPSM_Template::getAllTemplateHeaders () as $template ) {
																																									echo '<option value="' . $template->ID . '">' . $template->title . '</option>';
																																								}
																																								?>
                                    </select> <img src=<?php echo WPSM_Plugin_Integrator::imageUrl("delete-medium.png")?>
									class="mail-item-delete" alt="Remove
                                    button" title="Remove this mail item" /></li>
							</ul>
							<ul id="validation-errors">
								<li>Please input the offset days for each mail item.</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" style="float: right;" name="submit" id="submit" class="button-primary" value="Save Changes" />
						<h3>License</h3></td>
				</tr>
				<tr>
					<th scope="row"><label for="license_email"><?php _e("License Email", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" name="license_email" style="width: 100%'" value="<?php echo format_to_edit($wpsm_license_email); ?>"></td>
				</tr>
				<tr>
					<th scope="row"><label for="license_key"><?php _e("License Key", WPSM_I18N_DOMAIN); ?></label></th>
					<td><input type="text" name="license_key" style="width: 100%'" value="<?php echo format_to_edit($wpsm_license_key); ?>">
						<p class="description">
							You need to get this key from the <a href="http://wpsupermail.com/">SuperMail site</a>.
						</p></td>
				</tr>
			</table>
		</form>
		<div id="wpsm-bottom-error-container">
			<h4 class="error">
				SuperMail cannot update its settings. Please check fields marked with <span class="inline-error">*</span>.
			</h4>
		</div>
	</div>
</div>
