<?php
require_once $wpsm_base_path . "constants.php";

class WPSM_Plugin_Integrator
{
    const PLUGIN_DIR = "supermail";
    private $_basePath = "";
    private $_logger;

    public static function getVersion()
    {
        return '0.10';
    }

    public static function scriptUrl($scriptPath)
    {
        return strtolower(plugins_url() . '/' . self::PLUGIN_DIR . '/js/' . $scriptPath);
    }

    public function getScriptUrl($scriptPath)
    {
        return strtolower(plugins_url() . '/' . self::PLUGIN_DIR . '/js/' . $scriptPath);
    }

    public static function imageUrl($imagePath)
    {
        return strtolower(plugins_url() . '/' . self::PLUGIN_DIR . '/images/' . $imagePath);
    }

    public static function styleUrl($imagePath)
    {
        return strtolower(plugins_url() . '/' . self::PLUGIN_DIR . '/styles/' . $imagePath);
    }

    public function __construct()
    {
        global $wpsm_base_path;
        $this->_logger = new WPSM_Logger();
        $this->_basePath = $wpsm_base_path;
    }

    public function activate()
    {
        $scheduler = new WPSM_Scheduler ();
        $interval = get_option(WPSM_MAIL_PROCESS_INTERVAL, 30);
        $scheduler->addScheduledHook(WPSM_CRONHOOK_MAIL, $interval, array('WPSM_SuperMail', 'processQueue'));

        add_action('init', array($this, 'action_init'));
        add_action('admin_menu', array($this, 'action_admin_menu'));
        add_action('add_meta_boxes', array($this, 'action_add_meta_boxes'));
        add_action('save_post', array($this, 'action_save_post'));
        add_action('show_user_profile', array($this, 'action_show_user_profile'));
        add_action('personal_options_update', array($this, 'action_personal_options_update'));
        add_action('user_register', array($this, 'action_user_register'));

        // TODO Look at only hooking actions like this when the queue page is loaded.
        add_action('wp_ajax_process_queue', array($this, 'processQueue'));
        add_action('wp_ajax_clear_queue', array($this, 'clearQueue'));
        add_action('admin_notices', array($this, 'action_license_check'));
        add_filter('authenticate', array($this, 'filter_authenticate'), 10, 2);
        add_filter('pre_get_posts', array($this, 'filter_pre_get_posts'));
        add_action('publish_post', array($this, 'action_publish_posts'));
    }

    public function deactivate()
    {
        error_log("Deactivated.");
        wp_clear_scheduled_hook('process_queue_event');
    }

    private function mail_register_post_type()
    {
        $labels = array('name' => 'Mail Templates', 'singular_name' => 'SuperMail Template', 'add_new' => 'New Template', 'add_new_item' => 'New SuperMail Template', 'edit_item' => 'Edit SuperMail Template', 'new_item' => 'New SuperMail Template', 'view_item' => 'View Template', 'search_items' => 'Search Templates', 'not_found' => 'No SuperMail Templates Found', 'not_found_in_trash' => 'No SuperMail Templates Found In Trash');
        $supports = array('title', 'editor', 'revisions');
        $args = array('label' => 'Mail Template', 'description' => 'Mail template for SuperMail', 'show_ui' => true, 'show_in_menu' => true, 'can_export' => true, 'labels' => $labels, 'supports' => $supports);
        $reg = register_post_type(WPSM_MAIL_POST_TYPE, $args);
        if (is_a($reg, 'WP_Error')) {
            die ($reg->errors [0]);
        }
    }

    public function action_init()
    {
        $this->mail_register_post_type();
    }

    public function processQueue()
    {
        $this->_logger->log_message("Integrator::processQueue run by scheduler");
        $supermail = new WPSM_SuperMail();
        $supermail->processQueue();
    }

    public function clearQueue()
    {
        // TODO Get this into an Ajax handling class somewhere.
        // TODO Return a JSON object with header and data, for returning error codes etc.
        $wpsm_queue = new WPSM_MailQueue ();
        $wpsm_queue->ClearQueue();
        echo WPSM_MailQueue::GetDisplayItemsHtml();
        die ();
    }

    public function submenu_show_send_mail()
    {
        global $wpsm_plugin_object;
        require_once $this->_basePath . "send-mail.php";
    }

    public function submenu_show_options()
    {
        global $wpsm_plugin_object;
        require_once $this->_basePath . "settings.php";
    }

    public function submenu_show_mail_queue()
    {
        global $wpsm_plugin_object;
        require_once $this->_basePath . "mail-queue.php";
    }

    public function action_license_check()
    {
        // FIXME Well.
        return true;
    }

    public function action_admin_menu()
    {
        add_menu_page(__('SuperMail', WPSM_I18N_DOMAIN), __('SuperMail', WPSM_I18N_DOMAIN), 'manage_options', __FILE__, '');
        add_submenu_page(__FILE__, __('SuperMail', WPSM_I18N_DOMAIN), __('Send Mail', WPSM_I18N_DOMAIN), 'manage_options', __FILE__, array($this, 'submenu_show_send_mail'));
        add_submenu_page(__FILE__, __('SuperMail', WPSM_I18N_DOMAIN), __('Settings', WPSM_I18N_DOMAIN), 'manage_options', __FILE__ . '_options', array($this, 'submenu_show_options'));
        add_submenu_page(__FILE__, __('SuperMail', WPSM_I18N_DOMAIN), __('Mail Queue', WPSM_I18N_DOMAIN), 'manage_options', __FILE__ . '_queue', array($this, 'submenu_show_mail_queue'));
        unset ($GLOBALS ['submenu'] [__FILE__] [0]);
    }

    public function action_add_meta_boxes()
    {
        // require_once 'class-wpsm-template.php';
        add_meta_box('wpsm-mail-meta', 'Template Info', 'WPSM_Template::createTemplateMetaBox', WPSM_MAIL_POST_TYPE, 'normal', 'high');
    }

    public function action_save_post($post_id)
    {
        $post = get_post($post_id);
        if (($post->post_type == WPSM_MAIL_POST_TYPE) && ($post->post_status == "publish")) {
            WPSM_Template::saveTemplateMeta($post_id);
        }
    }

    public function action_publish_posts($post_ID)
    {

        $supermail = new WPSM_SuperMail ();
        $supermail->sendNewPostNotification($post_ID);
    }

    public function action_show_user_profile()
    {
        require_once $this->_basePath . "user-profile.php";
    }

    public function action_personal_options_update()
    {
        // require_once '\..\wpsm-user-profile.php';
        require_once $this->_basePath . "user-profile.php";
        wpsm_user_profile_update();
    }

    public function action_user_register($user_id)
    {
        add_user_meta($user_id, WPSM_USER_META_ISNEW, true, true);
    }

    // public function action_process_queue() {
    // // TODO Get this into an Ajax handling class somewhere.
    // // TODO Return a JSON object with header and data, for returning error codes etc.
    // $wpsm_queue = new WPSM_MailQueue ();
    // $process_size = isset ( $_POST ['process_size'] ) ? $_POST ['process_size'] : $wpsm_queue->GetMaxBatchSize ();
    // WPSM_SuperMail::processQueue ( $process_size );
    // echo WPSM_MailQueue::GetDisplayItemsHtml ();
    // die ();
    // }



    public function filter_authenticate($user, $user_name)
    {
        $user = get_user_by('login', $user_name);
        if ($user_name != '') {
            $isNew = ( bool )get_user_meta($user->ID, WPSM_USER_META_ISNEW, true);
            if ($isNew) {
                WPSM_SuperMail::sendNewUserMailSeries($user->ID);
                delete_user_meta($user->ID, WPSM_USER_META_ISNEW, true);
            }
        }
    }

    public function filter_pre_get_posts($query)
    {
        if (is_admin() && (isset ($query->query_vars ['post_type']))) {
            if ($query->query_vars ['post_type'] == WPSM_MAIL_POST_TYPE) {
                $query->set('meta_key', 'is_ad_hoc');
                $query->set('meta_compare', '!=');
                $query->set('meta_value', true);
            }
        }
    }

    public static function getUserRoles()
    {
        global $wp_roles;

        if (!isset ($wp_roles))
            $wp_roles = new WP_Roles ();

        return $wp_roles->get_names();
    }
}
?>