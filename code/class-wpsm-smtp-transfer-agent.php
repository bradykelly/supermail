<?php
require_once $wpsm_base_path . "constants.php";

class WPSM_Smtp_Transfer_Agent {
	
	private $_host = 'localhost';
	private $_port = 25;
	private $_encryption = 'none';
	private $_authenitcate = 'no';
	private $_user_name = '';
	private $_password = '';
	
	public function __construct() {
	
	}
	
	public static function listEncryptionStreams() {
		return array ('None', 'SSL', 'TLS' );
	}
	
	public function getHost() {
		return $this->_host;
	}
	public function setHost($value) {
		$this->_host = $value;
	}
	
	public function getPort() {
		return $this->_port;
	}
	public function setPort($value) {
		$this->_port = $value;
	}
	
	public function getEncryption() {
		return $this->_encryption;
	}
	public function setEncryption($value) {
		$this->_encryption = $value;
	}
	
	public function getAuthenticate() {
		return $this->_authenticate;
	}
	public function setAuthenticate($value) {
		$this->_authenticate = $value;
	}
	
	public function getUserName() {
		return $this->_user_name;
	}
	public function setUserName($value) {
		$this->_user_name = $value;
	}
	
	public function getPassword() {
		return $this->_password;
	}
	public function setPassword($value) {
		$this->_password = $value;
	}
}

?>