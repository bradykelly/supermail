// <?php
// define ( 'WPSM_PLUGIN_TITLE', 'SuperMail' );
// define ( 'WPSM_USER_META_REFUSE_BULK_MAIL', 'wpsm_refuse_bulk_mail' );
// define ( 'WPSM_USER_META_ISNEW', 'wpsm_new_user' );
// define ( 'WPSM_I18N_DOMAIN', 'supermail' );
// define ( 'WPSM_MAIL_SEND_STYLES', 'wpsm_mail_send' );
// define ( 'WPSM_MAIL_POST_TYPE', 'wpsm_mail_template' );
// define ( 'WPSM_MAIL_META_SENDER', 'wpsm_mail_sender' );
// define ( 'WPSM_MAIL_META_TITLE', 'wpsm_mail_title' );
// define ( 'WPSM_MAIL_META_SUBJECT', 'wpsm_mail_subject' );
// define ( 'WPSM_MAIL_META_ATTACHMENT', 'wpsm_mail_attachment' );
// define ( 'WPSM_OPTION_DEFAULT_FROM', 'wpsm_default_from' );
// define ( 'WPSM_OPTION_WELCOME_SERIES', 'wpsm_welcome_series' );
// define ( 'WPSM_DB_MAILQUEUE_TABLE', 'supermail_queue' );
// define ( 'WPSM_MAIL_AGENT_TYPE', 'wpsm_agent_type' );
// define ( 'WPSM_MAIL_AGENT_SMTP_HOST', 'wpsm_smtp_host' );
// define ( 'WPSM_MAIL_AGENT_SMTP_PORT', 'wpsm_smtp_port' );
// define ( 'WPSM_MAIL_AGENT_SMTP_ENCRYPT', 'wpsm_smtp_encrypt' );
// define ( 'WPSM_MAIL_AGENT_SMTP_USER_NAME', 'wpsm_smtp_user_name' );
// define ( 'WPSM_MAIL_AGENT_SMTP_PASSWORD', 'wpsm_smtp_password' );
// define ( 'WPSM_MAIL_AGENT_SENDMAIL_COMMAND', 'wpsm_sendmail_command' )?>