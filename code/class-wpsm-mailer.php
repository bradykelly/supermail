<?php

/**
 * Description of wpsm-class-mailer2
 *
 * @author Brady
 */

class WPSM_Mailer
{

    private $swift_mailer;

    public function __construct()
    {
        global $wpsm_base_path;

        require_once $wpsm_base_path . "constants.php";
        require_once 'swift_required.php';

        switch (get_option(WPSM_MAIL_AGENT_TYPE)) {
            case 'PHP' :
                $transport = Swift_MailTransport::newInstance();
                break;
            case 'SendMail' :
                $transport = Swift_SendmailTransport::newInstance();
                $transport->setCommand(get_option(WPSM_MAIL_AGENT_SENDMAIL_COMMAND));
                break;
            case 'SMTP' :
                $transport = Swift_SmtpTransport::newInstance();
                $transport->setHost(get_option(WPSM_MAIL_AGENT_SMTP_HOST));
                $transport->setPort(get_option(WPSM_MAIL_AGENT_SMTP_PORT));
                $enc = get_option(WPSM_MAIL_AGENT_SMTP_ENCRYPT);
                if ((strlen($enc) > 0) && (strtolower($enc) !== "none")) {
                    $transport->setEncryption($enc);
                }
                $user_name = get_option(WPSM_MAIL_AGENT_SMTP_USER_NAME);
                $password = get_option(WPSM_MAIL_AGENT_SMTP_PASSWORD);
                if (strlen($user_name) > 0) {
                    error_log("SMTP User name is $user_name");
                    error_log("SMTP Password is $password");
                    $transport->setUsername($user_name);
                    $transport->setPassword(get_option(WPSM_MAIL_AGENT_SMTP_PASSWORD));
                }
                break;
            default :
                $transport = Swift_MailTransport::newInstance();
                break;
        }
        $this->swift_mailer = Swift_Mailer::newInstance($transport);
    }

    public function sendTemplateMail(WPSM_Template $template, WPSM_Recipient $recipient)
    {

        $message = Swift_Message::newInstance();
        $message->setFrom($template->from);
        $message->setTo($recipient->email);
        $message->setSubject($template->subject);

        $supermail = new WPSM_SuperMail ();
        // $body = sprintf ( "%s\n\n\n\n<span style='color: #808080; font-size: small; font-style: italic;'>%s\n%s</span>", $template->body,
        // $supermail->getLegalAddress (), $supermail->getSubscriptionFooter () );
        $body = sprintf("%s\n\n\n\n%s\n%s", $template->body, $supermail->getLegalAddress(), $supermail->getSubscriptionFooter());
        $message->setBody($body);
        $att_path = $template->attachment_path;
        if (isset ($att_path) && strlen($att_path) > 0) {
            $attachment = Swift_Attachment::fromPath($att_path);
            $message->attach($attachment);
            error_log("Meant to attach: $att_path");
        }
        $sent = $this->swift_mailer->send($message);
        if ($sent > 0) {
            return 'Sent';
        }
        return 'Failed';
    }

    public function sendTestMail($recipient)
    {

        $message = Swift_Message::newInstance();
        $from = get_option(WPSM_OPTION_DEFAULT_FROM);
        if (strlen($from) == 0) {
            $from = get_option('admin_email');
        }
        $message->setFrom($from);
        $message->setTo($recipient);
        $message->setSubject('SuperMail Test');
        $supermail = new WPSM_SuperMail ();
        $body = sprintf("Mail transport test for agent: %s \n\n\n\n %s\n", get_option(WPSM_MAIL_AGENT_TYPE), $supermail->getLegalAddress());
        $body .= $supermail->getSubscriptionFooter();
        $message->setBody($body);

        try {
            $sent = $this->swift_mailer->send($message);
            error_log("Test mail sent.");
        } catch (Exception $e) {
            error_log($e->getTraceAsString());
            $sent = 0;
        }
        return $sent;
    }
}

?>
