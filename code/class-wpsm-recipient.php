<?php

class WPSM_Recipient {
	
	public function __construct($wp_user) {
		
		$this->_user_id = $wp_user->ID;
		
		$this->_email = $wp_user->user_email;
		
		ini_set ( 'display_errors', 0 );
		// TODO Bring these back later.
		// $this->_first_name = $wp_user->user_firstname;
		// $this->_last_name = $wp_user->user_lastname;
		// $this->_nickname = $wp_user->nickname;
		ini_set ( 'display_errors', 1 );
	
	}
	
	public function __get($name) {
		
		$method = 'get_' . $name;
		
		return $this->$method ();
	
	}
	
	public function __set($name, $value) {
		
		$method = 'set_' . $name;
		
		return $this->$method ( $value );
	
	}
	
	public static function getRecipientById($id) {
		
		$wp_user = get_userdata ( $id );
		
		if (is_null ( $wp_user )) {
			
			trigger_error ( "WPSM_Recipient::getRecipientById found no data for recipient ID $id.", E_USER_NOTICE );
			
			return null;
		
		}
		
		return new WPSM_Recipient ( $wp_user );
	
	}
	
	/**
	 *
	 * @return multitype:WPSM_Recipient
	 */
	
	public static function getAllRecipients() {
		
		$args = array ("blog_id" => $GLOBALS ['blog_id'] );
		
		$user_list = get_users ( $args );
		
		$recip_list = array ();
		
		foreach ( $user_list as $u ) {
			
			// TODO See if I can use this function with an optional meta criteria, or replace with DB query. This is inefficient.
			$refuse = get_user_meta ( $u->ID, WPSM_USER_META_REFUSE_BULK_MAIL, true );
			
			if (! $refuse) {
				
				$r = new WPSM_Recipient ( $u );
				
				$recip_list [] = $r;
			
			}
		
		}
		
		return $recip_list;
	
	}
	
	private $_user_id;
	
	function get_user_id() {
		
		return $this->_user_id;
	
	}
	
	function set_user_id($value) {
		
		$this->_user_id = $value;
	
	}
	
	private $_email;
	
	function get_email() {
		
		return $this->_email;
	
	}
	
	function set_email($value) {
		
		$this->_email = $value;
	
	}
	
	private $_first_name;
	
	function get_first_name() {
		
		return $this->_first_name;
	
	}
	
	function set_first_name($value) {
		
		$this->_first_name = $value;
	
	}
	
	private $_last_name;
	
	function get_last_name() {
		
		return $this->_last_name;
	
	}
	
	function set_last_name($value) {
		
		$this->_last_name = $value;
	
	}
	
	private $_nickname;
	
	function get_nickname() {
		
		return $this->_nickname;
	
	}
	
	function set_nickname($value) {
		
		$this->_nickname = $value;
	
	}

}

?>
