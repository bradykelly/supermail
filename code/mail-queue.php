<?php
require_once 'supermail.php';

// wp_enqueue_style ( 'QueueStyle', WPSM_Plugin_Integrator::styleUrl ( 'mail-queue.css' ) );
// wp_enqueue_script ( 'QueueScript', WPSM_Plugin_Integrator::scriptUrl ( 'mail-queue.js' ) );

$wpsm_queue = new WPSM_MailQueue ();
$wpsm_process_status = $wpsm_queue->GetPollingStatus ();
$wpsm_process_size = $wpsm_queue->GetMaxBatchSize ();
$wpsm_process_desc = $wpsm_queue->GetProcessDesc ();

?>
<div id="queue-wrap" class="wrap">
	<h2>SuperMail <?php echo 'v' . $wpsm_plugin_object->getVersion() . ' Dashboard'; ?></h2>
	<h3><?php _e('Mail Queue - Unsent Items', WPSM_I18N_DOMAIN); ?></h3>
	<div id="queue-update">
		<table id="mail_queue" class="wp-list-table">
			<thead>
				<tr>
					<th scope="col" class="table-column" id="priority">Priority</th>
					<th scope="col" class="table-column" id="date_to_send">To be Sent</th>
					<th scope="col" class="table-column" id="user_login">User</th>
					<th scope="col" class="table-column" id="post_title">Template</th>
					<th scope="col" class="table-column" id="status_desc">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ( WPSM_MailQueue::GetDisplayItems () as $key => $item ) {
					$item->status_desc = isset ( $item->status_desc ) ? $item->status_desc : "Queued";
					$css_class = "status-" . strtolower ( $item->status_desc );
					echo "<tr task_id='$key'>\n";
					echo "<td>$item->priority</td>\n";
					echo "<td>$item->date_to_send</td>\n";
					echo "<td>$item->user_login</td>\n";
					echo "<td>$item->post_title</td>\n";
					echo "<td class='$css_class'>$item->status_desc</td>\n";
					echo "</tr>\n";
				}
				?>
			</tbody>
		</table>
	</div>
	<h3><?php _e('Automated Queue Processing', WPSM_I18N_DOMAIN); ?></h3>
	<ul id="queue-status">
		<li>Automated queue processing is: <?php echo $wpsm_process_status; ?>.</li>
		<li>The queue is processed every: <?php echo $wpsm_process_desc; ?>.</li>
		<li>The maximum number of mails sent at once is: <?php echo $wpsm_process_size; ?>.</li>
	</ul>
	<h3><?php _e('Explicit Queue Processing', WPSM_I18N_DOMAIN); ?></h3>
	<ul id="send-now_commands">
		<li class="send-command"><a href="#" id="queue-process"
			class="command">Click here </a>&nbsp;to process <input type="text"
			name="process_size" id="process_size" style="width: 30px;"
			value="<?php echo $wpsm_process_size; ?>" /> mail tasks right now. <img
			id="queue-process-ajax"
			src="<?php echo WPSM_Plugin_Integrator::imageUrl('spinner-small.gif'') ?>" /></li>
		<li class="send-command hidden">
			<button>Click to...</button> Send all unsent items.
		</li>
		<li class="send-command hidden">
			<button>Click to...</button> Send all selected items.
		</li>
		<li class="send-command hidden">
			<button>Click to...</button> Suspend automated queue processing.
		</li>
		<li class="send-command hidden">
			<button>Click to...</button> Resume automated queue processing.
		</li>
		<li class="send-command"><a href="#" id="queue-clear" class="command">Click
				here </a><span style="color: red;">&nbsp;<strong>to clear the queue.
					No undo. No turning back. This. Is. It!</strong></span><img
			id="queue-clear-ajax"
			src=<?php echo WPSM_Plugin_Integrator::imageUrl("spinner-small.gif"); ?> />
		</li>
	</ul>
</div>