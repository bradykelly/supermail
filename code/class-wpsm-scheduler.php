<?php
require_once $wpsm_base_path . "constants.php";
class WPSM_Scheduler {
	// TODO Implement persistence.
	
	private $logger;
	private $hooks = array ();
	
	private static $measures = array ('minutes' => 60, 'hours' => 3600, 'days' => 86400 );
	
	public static function getIntervalMeasures() {
		return self::$measures;
	}
	
	public static function getIntervalSeconds($measureName, $measureCount) {
		if (array_key_exists ( $measureName, self::$measures )) {
			return self::$measures [$measureName] * $measureCount;
		}
		return null;
	}
	
	public function __construct(WPSM_Logger $injected_logger = null) {
		if (! isset ( $injected_logger )) {
			$injected_logger = new WPSM_Logger ();
		}
		$this->logger = $injected_logger;
		add_filter ( 'cron_schedules', array ($this, 'filterCronSchedules' ) );
	}
	
	public function addScheduledHook($hookName, $interval, $callback) {
		// Our filter must have the required schedule before we can call 'wp_schedule_event'.
		if (! array_key_exists ( $hookName, $this->hooks )) {
			$this->hooks [$hookName] = array ("interval" => $interval, "callback" => $callback );
		}
		if (! wp_next_scheduled ( $hookName )) {
			wp_schedule_event ( time (), $hookName, $hookName );
			$this->logger->log_message ( "Added hook" );
		}
		add_action ( $hookName, $callback );
	}
	
	public function changeScheduledHook($hookName, $interval) {
		// Our filter must have the required schedule before we can call 'wp_schedule_event'. Don't need 'callback' here, just 'interval'.
		if (! array_key_exists ( $hookName, $this->hooks )) {
			$this->hooks [$hookName] = array ("interval" => $interval );
		}
		if (wp_next_scheduled ( $hookName )) {
			wp_reschedule_event ( $hookName, $interval );
		}
	}
	
	public function removeScheduledHook($hookName) {
		if (wp_next_scheduled ( $hookName )) {
			wp_clear_scheduled_hook ( $hookName );
			$this->logger->log_message ( "Removed hook" );
		}
		unset ( $this->hooks [$hookName] );
	}
	
	public function filterCronSchedules($schedules) {
		foreach ( $this->hooks as $name => $config ) {
			$schedules [$name] = array ('interval' => $config ['interval'], 'display' => $name );
		}
		return $schedules;
	}
	
	public function processQueue() {
		$this->logger->log_message ( "Scheduler::processQueue run by scheduler" );
	}
}
?>