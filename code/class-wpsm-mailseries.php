<?php

class WPSM_MailSeries {
	
	private $items;
	
	function __construct() {
	
	}
	
	public function deserializeJson($json) {
		$this->items = array ();
		$raw = json_decode ( $json );
		foreach ( $raw as $item ) {
			$this->items [] = new WPSM_MailSeriesItem ( $item->offset, $item->templateId );
		}
	}
	
	public function getItems() {
		return $this->items;
	}
}

?>