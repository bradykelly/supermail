<?php

require_once 'class-wpsm-mailsendtask.php';

/**
 * Description of class-wpsm-mailqueue
 *
 * @author Brady
 */
class WPSM_MailQueue {
	
	public static function enqueue(WPSM_MailSendTask $mail_task) {
		global $wpdb;
		
		$col_vals = array ("date_queued" => current_time ( 'mysql' ), "template_id" => $mail_task->TemplateId, "recipient_id" => $mail_task->RecipientId, "date_to_send" => $mail_task->DateToSend, "priority" => $mail_task->Priority );
		$ret = $wpdb->insert ( $wpdb->prefix . WPSM_DB_MAILQUEUE_TABLE, $col_vals );
	}
	
	public static function dequeue($size) {
		global $wpdb;
		
		$count = ( int ) $size;
		if ((! is_int ( $count ))) {
			$count = 1;
		}
		
		$sql = "SELECT * FROM `wp_supermail_queue` ";
		$sql .= "where 1=1 ";
		$sql .= "and ((status_desc is null) or ((status_desc <> 'Hold') and (status_desc  <> 'Failed'))) ";
		$sql .= "and (date_sent is null) ";
		$sql .= "and ((status_desc is null) or (status_desc <> 'Hold')) ";
		$sql .= "order by if(status_desc = 'Forced', -99,priority), date_to_send, date_queued ";
		$sql .= "limit $count ";
		return $wpdb->get_results ( $sql, OBJECT_K );
	}
	
	public static function MarkItemsBusy(array $tasks) {
		global $wpdb;
		error_log ( serialize ( $tasks ) );
		// die(count($tasks));
		foreach ( $tasks as $id => $task ) {
			error_log ( sprintf ( "Updating %d", $id ) );
			$wpdb->update ( $wpdb->prefix . WPSM_DB_MAILQUEUE_TABLE, array ('status_desc' => 'Busy' ), array ('task_id' => $id ) );
		}
	}
	
	public static function UpdateItemStatus($task_id, $status_desc) {
		global $wpdb;
		$col_vals = array ('status_desc' => $status_desc );
		if ($status_desc === 'Sent') {
			$col_vals ['date_sent'] = date ( 'Y-m-d H:i:s' );
		}
		$wpdb->update ( $wpdb->prefix . WPSM_DB_MAILQUEUE_TABLE, $col_vals, array ('task_id' => $task_id ) );
	}
	
	public static function GetDisplayItems() {
		global $wpdb;
		
		$sql = "SELECT smq.*, wpp.post_title, wpu.user_login ";
		$sql .= "FROM wp_supermail_queue smq ";
		$sql .= "INNER JOIN wp_posts wpp ON smq.template_id = wpp.ID ";
		$sql .= "INNER JOIN wp_users wpu ON smq.recipient_id = wpu.ID ";
		$sql .= "WHERE (date_sent is null) ";
		$sql .= "AND ((status_desc is null) or (status_desc <> 'Sent')) ";
		$sql .= "order by if(status_desc = 'Forced', -99,priority), date_to_send, date_queued\n";
		// $sql .= "limit $count";
		// echo $sql;
		return $wpdb->get_results ( $sql, OBJECT_K );
	}
	
	// TODO Must move this away from the queue to a UI clas.
	public static function GetDisplayItemsHtml() {
		global $wpdb;
		
		$html_rows = "";
		$items = self::GetDisplayItems ();
		if (count ( $items ) == 0) {
			return "<td colspan='5' class='empty-queue'>No mail tasks are in the queue.</td>\n";
		}
		foreach ( $items as $key => $item ) {
			$item->status_desc = isset ( $item->status_desc ) ? $item->status_desc : "Queued";
			$css_class = "status-" . strtolower ( $item->status_desc );
			$html_rows .= "<tr task_id='$key'>\n";
			$html_rows .= "<td>$item->priority</td>\n";
			$html_rows .= "<td>$item->date_to_send</td>\n";
			$html_rows .= "<td>$item->user_login</td>\n";
			$html_rows .= "<td>$item->post_title</td>\n";
			$html_rows .= "<td class='$css_class'>$item->status_desc</td>\n";
			$html_rows .= "</tr>\n";
		}
		return $html_rows;
	}
	
	public function GetMaxBatchSize() {
		// TODO Refactor this out, queue is not responsible for processing itself.
		return wp_strip_all_tags ( get_option ( WPSM_MAIL_PROCESS_SIZE ) );
	}
	
	public function GetProcessFrequency() {
		// TODO Don't always read, use values from constructor.
		return wp_strip_all_tags ( get_option ( WPSM_MAIL_PROCESS_FREQUENCY ) );
	}
	
	public function GetProcessInterval() {
		// TODO Don't always read, use values from constructor.
		return wp_strip_all_tags ( get_option ( WPSM_MAIL_PROCESS_MEASURE ) );
	}
	
	public function GetProcessDesc() {
		$freq = $this->GetProcessFrequency ();
		$interval = $this->GetProcessInterval ();
		if ($freq > 1) {
			switch ($interval) {
				case "day" :
					return $freq . " days";
				case "hour" :
					return $freq . " hours";
			}
		}
		return $freq . " " . $interval;
	}
	
	public function GetPollingStatus() {
		return "Inactive";
	}
	
	public function ClearQueue() {
		global $wpdb;
		
		// TODO Use a Deleted flag. Never really delete anything.
		$table = $wpdb->prefix . "wp_supermail_queue";
		$wpdb->query ( "DELETE FROM $table" );
	}
}
?>
