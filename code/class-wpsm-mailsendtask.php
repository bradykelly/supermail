<?php

/**
 * Description of WPSM_SendMailTask
 *
 * @author Brady
 */
class WPSM_MailSendTask {

    private $_task_id;
    private $_date_queued;
    private $_template_id;
    private $_recipient_id;
    private $_date_to_send;
    private $_priority;
    private $_date_sent;

    public function __construct($template_id, $recipient_id, $date_to_send = null, $priority = null) {

        if ($date_to_send ==  null) {
            $date_to_send = current_time('mysql');
        }
        if ($priority == null) {
            $priority = 0;
        }

        $this->_template_id = $template_id;
        $this->_recipient_id = $recipient_id;
        $this->_date_to_send = $date_to_send;
        $this->_priority = $priority;
    }

    public function __get($name) {
        $method = 'get' . $name;
        return $this->$method();
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        return $this->$method($value);
    }

    private function getTemplateId() {
        return $this->_template_id;
    }

    private function setTemplateId($value) {
        $this->_template_id = $value;
    }

    private function getRecipientId() {
        return $this->_recipient_id;
    }

    private function setRecipientId($value) {
        $this->_recipient_id = $value;
    }

    private function getDateToSend() {
        return $this->_date_to_send;
    }

    private function setDateToSend($value) {
        $this->_date_to_send = $value;
    }

    private function getPriority() {
        return $this->_priority;
    }

    private function setPriority($value) {
        $this->_priority = $value;
    }

    private function getDateSent() {
        return $this->_date_sent;
    }

    private function setDateSent($value) {
        $this->_date_sent = $value;
    }
}

?>
