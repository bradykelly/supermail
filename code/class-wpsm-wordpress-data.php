<?php

class WPSM_WordPressData {
	
	public static function CreateQueueTable() {
		global $wpdb;
		
		$name = $wpdb->prefix . "supermail_queue";
		$sql = "CREATE TABLE `$name` ( 
				`task_id` int(11) NOT NULL AUTO_INCREMENT,
				`date_queued` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`template_id` int(11) NOT NULL,
				`recipient_id` int(11) NOT NULL,
				`date_to_send` datetime DEFAULT NULL,
				`priority` int(11) NOT NULL DEFAULT '0',
				`date_sent` datetime DEFAULT NULL,
				`status_desc` varchar(20) DEFAULT NULL,
				KEY `wp_supermail_queue.PRIMARY` (`task_id`)
		) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1; ";
		
		require_once (ABSPATH . 'wp-admin/includes/upgrade.php');
		error_log($sql);
		dbDelta ( $sql );
	}
}

?>