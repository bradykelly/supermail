<?php
require_once 'Date.php';
class WPSM_SuperMail {
	
	public static function getInstalledVersion() {
		$plugin_data = get_plugin_data ( __FILE__ );
		return $plugin_data ['Version'];
	}
	
	public function getLegalAddress() {
		$address = trim ( wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_NAME ) ) );
		$address .= ', ' . trim ( wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_ADDRESS ) ) );
		$address .= ', ' . trim ( wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_STATE ) ) );
		$address .= ', ' . trim ( wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_ZIP ) ) );
		$address .= ', ' . trim ( wp_strip_all_tags ( get_option ( WPSM_OPTION_LEGAL_COUNTRY ) ) );
		return $address;
	}
	
	public function getSubscriptionFooter() {
		$blog_name = get_bloginfo ( 'name' );
		$footer = "This mail was sent to you because you are registered as a user at $blog_name. ";
		$profile_url = get_bloginfo ( 'url' ) . '/wp-admin/profile.php';
		$footer .= "\nPlease visit " . $profile_url . ' if you want to change your subscription options.';
		return $footer;
	}
	
	public static function sendAdHocMailToAllUsers($from, $subject, $body, $title, $attachment_path) {
		
		$template = WPSM_Template::createMailTemplate ( $from, $subject, $body, true, $title, $attachment_path );
		$recip_list = WPSM_Recipient::getAllRecipients ();
		foreach ( $recip_list as $recipient ) {
			$mail_task = new WPSM_MailSendTask ( $template->ID, $recipient->user_id );
			WPSM_MailQueue::enqueue ( $mail_task );
		}
	}
	
	public static function sendAdHocMailToUserGroup($group, $from, $subject, $body, $title, $attachment_path) {
		
		$template = WPSM_Template::createMailTemplate ( $from, $subject, $body, true, $title, $attachment_path );
		
		// TODO Move this to the Recipient class.
		$user_query = new WP_User_Query ( array ('role' => $group, 'fields' => 'all_with_meta' ) );
		$members = $user_query->get_results ();
		
		foreach ( $members as $m ) {
			$refuse = get_user_meta ( $m->ID, WPSM_USER_META_REFUSE_BULK_MAIL, true );
			if (! $refuse) {
				$recipient = new WPSM_Recipient ( $m );
				$mail_task = new WPSM_MailSendTask ( $template->ID, $recipient->user_id );
				WPSM_MailQueue::enqueue ( $mail_task );
			}
		}
	}
	
	public static function sendNewUserMailSeries($userId) {
		
		$json = get_option ( WPSM_OPTION_WELCOME_SERIES );
		$series = new WPSM_MailSeries ();
		$series->deserializeJson ( $json );
		
		// TODO Fix this kak.
		// Increment each mail by one second to avoid duplicate keys.
		$second_increment = 0;
		foreach ( $series->getItems () as $item ) {
			$dateToSend = new Date ();
			$dateToSend->addSpan ( new Date_Span ( "$item->offsetHours", '%d' ) );
			$dateToSend->addSpan ( new Date_Span ( "$second_increment", '%s' ) );
			$second_increment ++;
			$task = new WPSM_MailSendTask ( $item->templateId, $userId, $dateToSend->getDate (), 5 );
			WPSM_MailQueue::enqueue ( $task );
		}
	}
	
	public static function processQueue($batchSize = 1) {

        $logger = new WPSM_Logger();
		$mailer = new WPSM_Mailer ();

		$enabled = get_option(WPSM_MAIL_PROCESS_CRON, false);
		if(!$enabled) {
			return;
		}
        $logger->log_message("processQueue called.");
        return;


		$chosen = WPSM_MailQueue::dequeue ( $batchSize );
		$count = count ( $chosen );

		WPSM_MailQueue::MarkItemsBusy ( $chosen );
		foreach ( $chosen as $id => $item ) {
			$template = WPSM_Template::getTemplateById ( $item->template_id );
			if ((! isset ( $template->from )) || (strlen ( $template->from ))) {
				$template->from = esc_html ( get_option ( WPSM_OPTION_DEFAULT_FROM ) );
			}
			if ((! isset ( $template->from )) || (strlen ( $template->from ))) {
				$template->from = esc_html ( get_option ( 'admin_email' ) );
			}
			$recipient = WPSM_Recipient::getRecipientById ( $item->recipient_id );
			$status = $mailer->sendTemplateMail ( $template, $recipient );
			WPSM_MailQueue::UpdateItemStatus ( $id, $status );
		}
	}
	
	public static function checkLicense() {
		// TODO Fix this!
		return "true";
		
		$license_email = trim ( get_option ( 'wpsm_license_email' ) );
		$license_key = trim ( get_option ( 'wpsm_license_key' ) );
		if ((strlen ( $license_email ) > 0) && (strlen ( $license_key ) > 0)) {
			if (sha1 ( $license_email ) === $license_key) {
				return "true";
			}
		}
		
		// A good key will have caused an exit by now.
		return "false";
	}
	
	public function sendNewPostNotification($post_ID) {
		
		$post = get_post ( $post_ID );
		if (! isset ( $post )) {
			// TODO Publish this anomaly;
			return false;
		}
		
		if (($post->post_type != "post") && ($post->post_type != "page")) {
			return;
		}
		
		$blog_name = get_bloginfo ( 'name' );
		// TODO Make this a configed template.
		$subject = "New content on: " . $blog_name;
		$notice = "A new entry titled '" . $post->post_title . "' has been published on " . $blog_name;
		$notice .= "\n\nPlease click this link to read more: " . get_bloginfo ( 'url' );
		
		// TODO Maybe move this into mailer, or something between mailer and this.
		$from = esc_html ( get_option ( WPSM_OPTION_NEW_POST_TEMPLATE ) );
		if (strlen ( $from ) == 0) {
			$from = esc_html ( get_option ( 'admin_email' ) );
		}
		
		$body = $notice;
		$template = WPSM_Template::createMailTemplate ( $from, $subject, $body, true );
		
		$recip_list = WPSM_Recipient::getAllRecipients ();
		foreach ( $recip_list as $recipient ) {
			$mail_task = new WPSM_MailSendTask ( $template->ID, $recipient->user_id );
			WPSM_MailQueue::enqueue ( $mail_task );
		}
		error_log ( "Notified all users of new post: " . $post->post_title );
	}
}

?>