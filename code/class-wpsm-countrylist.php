<?php

class WPSM_CountryList {
	
	public static function listAll() {
		global $wpsm_base_path;
		
		$path = $wpsm_base_path . "code/countries.txt";
		$handle = fopen ( $path, "r" );
		$countries = array ("code" => null, "name" => "" );
		if ($handle) {
			
			while ( ! feof ( $handle ) ) {
				$parts = explode ( ":", fgets ( $handle ) );
				$country = array ("code" => $parts [0], "name" => $parts [1] );
				$countries [] = $country;
			}
		}
		fclose ( $handle );
		return $countries;
	}
}

?>