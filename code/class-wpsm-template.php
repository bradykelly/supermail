<?php

/**
 * Description of wpsm-class-template
 *
 * @author Brady
 */
class WPSM_Template {
	
	private $_ID;
	private $_title;
	private $_from;
	private $_subject;
	private $_attachment_path;
	private $_body;
	
	public function __construct($post, $get_body = false) {
		$this->_ID = $post->ID;
		$this->_title = $post->post_title;
		$this->_from = get_post_meta ( $post->ID, WPSM_MAIL_META_SENDER, true );
		$this->_subject = get_post_meta ( $post->ID, WPSM_MAIL_META_SUBJECT, true );
		$this->_attachment_path = get_post_meta ( $post->ID, WPSM_MAIL_META_ATTACHMENT, true );
		
		if ($get_body == true) {
			$this->_body = $post->post_content;
		}
	}
	
	public static function createMailTemplate($from, $subject, $body, $is_adhoc = false, $title = null, $attachment_path = null) {
		global $user_ID;
		
		// TODO Add warnings for argument validation.
		// TODO Consider non-adhoc with no title.
		// if (! isset ( $title )) {
		if ($is_adhoc) {
			$title = "Ad hoc: " . date ( 'U' );
			if (strlen ( $subject ) > 0) {
				$title .= ' - ' . $subject;
			}
		}
		
		error_log ( "Creating template with title: $title" );
		$new_post = array ('post_title' => $title, 'post_content' => $body, 'post_status' => 'private', 'post_date' => date ( 'Y-m-d H:i:s' ), 'post_author' => $user_ID, 'post_type' => WPSM_MAIL_POST_TYPE );
		$post_id = wp_insert_post ( $new_post );
		update_post_meta ( $post_id, 'is_ad_hoc', $is_adhoc );
		update_post_meta ( $post_id, WPSM_MAIL_META_SENDER, $from );
		update_post_meta ( $post_id, WPSM_MAIL_META_SUBJECT, $subject );
		if (isset ( $attachment_path )) {
			update_post_meta ( $post_id, WPSM_MAIL_META_ATTACHMENT, $attachment_path );
		}
		
		return new WPSM_Template ( get_post ( $post_id ), true );
	}
	
	public static function getTemplateById($template_id) {
		
		if (! isset ( $template_id )) {
			return null;
		}
		
		$post = get_post ( $template_id );
		if (is_null ( $post )) {
			error_log ( "WPSM_Template::getTemplateById failed for template ID $template_id." );
			return null;
		}
		$template = new WPSM_Template ( $post, true );
		return $template;
	}
	
	public static function getAllTemplateHeaders() {
		$args = array ("post_type" => WPSM_MAIL_POST_TYPE, "post_status" => "publish", "order_by" => "post_title", "order" => "ASC" );
		$post_list = get_posts ( $args );
		$template_list = array ();
		foreach ( $post_list as $p ) {
			$template_list [] = new WPSM_Template ( $p );
		}
		usort ( $template_list, array (__CLASS__, "sortRecipients" ) );
		
		return $template_list;
	}
	
	public static function sortRecipients($a, $b) {
		if ($a->title == $b->title) {
			return 0;
		}
		
		return ($a->title < $b->title) ? - 1 : 1;
	}
	
	public static function createTemplateMetaBox($post) {
		$wpint = new WPSM_Plugin_Integrator ();
		
		wp_enqueue_style ( 'template-style', WPSM_Plugin_Integrator::styleUrl ( 'template-metabox.css' ) );
		$jsurl = WPSM_Plugin_Integrator::scriptUrl ( 'template-meta-box.js' );
		wp_enqueue_script ( 'template-script', $jsurl );
		
		// TODO Pass a Template object, not a mob of variables, to the HTML.
		$title = $post->post_title;
		$from = get_post_meta ( $post->ID, WPSM_MAIL_META_SENDER, true );
		$subject = get_post_meta ( $post->ID, WPSM_MAIL_META_SUBJECT, true );
		$attachment = get_post_meta ( $post->ID, WPSM_MAIL_META_ATTACHMENT, true );
		require_once $wpint->_basePath . 'template-metabox.html';
	}
	
	public static function saveTemplateMeta($post_id, $is_adhoc = false) {
		
		// TODO Add some error detection and shit.
		if ((isset ( $_POST ['wpsm_mail_meta_update'] )) && ($_POST ['wpsm_mail_meta_update'] == 'true')) {
			if (! empty ( $_FILES [WPSM_MAIL_META_ATTACHMENT] ['name'] )) {
				$upload = wp_handle_upload ( $_FILES [WPSM_MAIL_META_ATTACHMENT], array ('test_form' => false ) );
				$nice_path = str_replace ( "\\", "/", $upload ['file'] );
				if ((isset ( $upload ['error'] )) && ($upload ['error'] != 'Specified file failed upload test.')) {
					$err = $upload ['error'];
					error_log ( "Attachment upload aborted: $err" );
					return;
				} else {
					update_post_meta ( $post_id, WPSM_MAIL_META_ATTACHMENT, $nice_path );
				}
			}
		}
		update_post_meta ( $post_id, 'is_ad_hoc', $is_adhoc );
		update_post_meta ( $post_id, WPSM_MAIL_META_SENDER, esc_html ( $_POST [WPSM_MAIL_META_SENDER] ) );
		update_post_meta ( $post_id, WPSM_MAIL_META_SUBJECT, esc_html ( $_POST [WPSM_MAIL_META_SUBJECT] ) );		
	}
	
	public function __get($name) {
		$method = 'get_' . $name;
		return $this->$method ();
	}
	
	public function __set($name, $value) {
		$method = 'set_' . $name;
		return $this->$method ( $value );
	}
	
	private function get_ID() {
		return $this->_ID;
	}
	
	private function set_ID($value) {
		$this->_ID = $value;
	}
	
	private function get_title() {
		return $this->_title;
	}
	
	private function set_title($value) {
		$this->_title = $value;
	}
	
	private function get_from() {
		return $this->_from;
	}
	
	private function set_from($value) {
		$this->_from = $value;
	}
	
	private function get_subject() {
		return $this->_subject;
	}
	
	private function set_subject($value) {
		$this->_subject = $value;
	}
	
	private function get_attachment_path() {
		return $this->_attachment_path;
	}
	
	private function set_attachment_path($value) {
		$this->_attachment_path = $value;
	}
	
	private function get_body() {
		return $this->_body;
	}
	
	private function set_body($value) {
		$this->_body = $value;
	}
}

?>
