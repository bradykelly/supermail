<?php

/**
 * Description of wpsm_Validator
 *
 * @author Brady
 */
class wpsm_Validator {

    private $error_messages;
    public function __construct() {
        $this->error_messages = array();
    }
    public function addErrorMessage($message) {
        $this->error_messages[] = $message;
    }    
    public function hasErrors() {
        return count($this->error_messages) > 0;
    }    
    public function getErrorMessages() {
        return $this->error_messages;
    }
}

?>
